package week2.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;


public class Windows {

	public static void main(String[] args) throws InterruptedException    {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://legacy.crystalcruises.com/");
		driver.findElementByLinkText("GUEST CHECK-IN").click();
		Set<String> allWindows = driver.getWindowHandles();
		for (String string : allWindows) {
			System.out.println(string);
		}
        List<String> list = new ArrayList<>();
        list.addAll(allWindows);
        driver.switchTo().window(list.get(1)); 
        driver.findElementByLinkText("View Offer�").click();
        allWindows = driver.getWindowHandles();
        List<String> list1 = new ArrayList<>();
        list1.addAll(allWindows);
        driver.switchTo().window(list1.get(2));
	

}
	}





