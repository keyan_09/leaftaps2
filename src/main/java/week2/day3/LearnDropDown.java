package week2.day3;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnDropDown {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Dropdown.html");
		Select sel = new Select(driver.findElementById("dropdown1"));
		sel.selectByIndex(3);
		List<WebElement> options = sel.getOptions();
		for (WebElement eachOp : options) {
			System.out.println(eachOp.getText());
		}
			WebElement source = driver.findElementByName("dropdown2");
			Select sel1= new Select(source);
			sel1.selectByVisibleText("Selenium");
			Select sel2=new Select(driver.findElementById("dropdown3"));
			sel2.selectByValue("2");
			
			Select sel3=new Select(driver.findElementByClassName("dropdown"));
			List<WebElement> options1 = sel3.getOptions();
			for (WebElement sel4 : options1) {
				System.out.println(sel4.getText());
				sel3.selectByIndex(1);
				
			//	driver.findElementByXPath("//option[text()='You can also use sendKeys to select']").sendKeys("d");
		
				
				
			}
			
		
			
		

	}

}
