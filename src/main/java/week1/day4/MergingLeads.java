package week1.day4;



import java.util.ArrayList;
import java.util.List;
import java.util.Set;

//import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergingLeads {
	
	@Test(timeOut=200000)
	
	
	public void mergingLeads() throws InterruptedException {

	//public static void main(String[] args) throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    	ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		Thread.sleep(2000);
		/*driver.findElementByXPath("((//input[@class='XdijitInputField dijitInpu tFieldValidationNormal'])[1])").sendKeys("10097");
		driver.findElementByXPath("((//input[@class='XdijitInputField dijitInputFieldValidationNormal'])[2])").sendKeys("10098");
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Thread.sleep(1000);
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@class=' x-form-text x-form-field'])[24]").sendKeys("10046");
		Thread.sleep(2000);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		String text = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		if (text.equals("No records to display")) {
			System.out.println("matched");
			
		} else {
			System.out.println("not matched");

		}
	
*/
		driver.findElementByXPath("(//div[@class='subSectionBlock']//img)[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> list= new ArrayList<>();
		list.addAll(windowHandles);
		driver.switchTo().window(list.get(1));
		String text = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a").getText();
		System.out.println(text);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]//a").click();
		//driver.switchTo().activeElement();
		driver.switchTo().window(list.get(0));
		driver.findElementByXPath("(//div[@class='subSectionBlock']//img)[2]").click();
		Thread.sleep(2000);
		windowHandles=driver.getWindowHandles();
		List<String> List1= new ArrayList<>();
		List1.addAll(windowHandles);
		driver.switchTo().window(List1.get(1));
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]//a").click();
		driver.switchTo().window(List1.get(0));
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		driver.switchTo().alert().accept();
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@class=' x-form-text x-form-field'])[24]").sendKeys(text);
		Thread.sleep(2000);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
	String text1 = driver.findElementByClassName("x-panel-bbar").getText();
	System.out.println(text1);
		
		if (text1.contains("No records to display")) {
			System.out.println("matched");
			
		} else {
			System.out.println("not matched");

		}
		
		
		
	}
}
