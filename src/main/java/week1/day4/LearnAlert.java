package week1.day4;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {
	public static void main(String[] args) throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Alert.html");
		driver.manage().window().maximize();
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Thread.sleep(5000);
		Alert alert = driver.switchTo().alert();
		driver.switchTo().alert().accept();
		String text = alert.getText();
		System.out.println(text);
		alert.sendKeys("paneer selvam");
		alert.accept();
		String text2 = driver.findElementByXPath("//p[@id='result1']").getText();
		if (text2.contains("paneer selvam")) {
			System.out.println("text matches");


		} else {
			System.out.println("text is not matched");
		}	

		



	}

}
