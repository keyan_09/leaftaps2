package winium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

public class WiniumCal {
	public static void main(String [] args) throws InterruptedException, MalformedURLException {
		DesktopOptions option=new DesktopOptions();
		option.setApplicationPath("C:\\Windows\\System32\\calc.exe");
		WiniumDriver driver = new WiniumDriver(new URL("http://localhost:9999"), option);
		Thread.sleep(2000);
		driver.findElement(By.name("One")).click();
		System.out.println("1 is clicked");

	}
}