



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeMethod implements WdMethods{
	public static void name() {
		
	}
	public RemoteWebDriver driver ;
	public void startApp(String browser, String url) {
		if (browser.equals("chrome")) {			
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equals("firefox")) {			
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}		
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched Successfully");
	}

	
	public WebElement locateElement(String locator, String locValue) {
		
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		}		
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("the Data "+data+" Endered Successfully");
	}

	@Override
	public void click(WebElement ele) {
	ele.click();
	System.out.println("the Element "+ele+" is clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {
		return ele.getText();
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
	if (driver.getTitle().contains(expectedTitle)) {
		System.out.println("the expected title is printed successfully");
		
	} else {
		System.out.println("the expected title is wrong");

	}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
	
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		
		
	}

	@Override
	public void switchToWindow(int index) {
		driver.switchTo().frame(index);
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		
	}

	@Override
	public String getAlertText() {
		 String textAlert = driver.switchTo().alert().getText();
		System.out.println("The alert Message is: "+ textAlert);
		return null;
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
	}

}
