package week3.day2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Collections {

	public static void main(String[] args) {
		List <String> list1 =new ArrayList<String>();
		list1.add("karthik");
		list1.add("gopaal");
		list1.add("viki");
		list1.add("oink oink");
		

		for(int i =0;i<list1.size();i++) {
			System.out.println(list1.get(i));
		}

		System.out.println("\n");

		list1.add(2, "pikachu");
		list1.add(0, "shruthi");
		Iterator<String> itr=list1.iterator();

		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		
		System.out.println("\n");
		LinkedList <String> list2= new LinkedList<>();
		list2.add("shruthi");
		list2.add("pikachu");
		list2.add("tamannah");
		list2.add("loshini");

		Iterator<String> itr2=list2.iterator();
		while(itr2.hasNext()) {
			System.out.println(itr2.next());
		}

		System.out.println("\n");
		list2.addAll(list1);
		list2.addFirst("karthik");
		list2.addLast("karthik");
		for(int i =0;i<list2.size();i++) {
			System.out.println(list2.get(i));
	
		}
	}
	

	
	
	
	
}
