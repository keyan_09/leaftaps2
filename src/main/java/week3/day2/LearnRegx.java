package week3.day2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LearnRegx {

	public static void main(String[] args) {
		//String value="amazonindia as an employee of 3450 in chennai ch-600028";
		//String pattern="\\d{4,}";
		 String value="psk1@gmail.com";
		 String pattern="[a-z0-9]+@[a-z]+\\.[a-z]";
		Pattern compile = Pattern.compile(pattern);
		Matcher matcher = compile.matcher(value);
		System.out.println(matcher.find());
	}

}
