package week3.day2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRead {

	
		public ArrayList<String> getData(String rowData) throws IOException {
		ArrayList<String> list=new ArrayList<>();
		
		FileInputStream file=new FileInputStream("./data/karthik.xlsx") ;

		XSSFWorkbook workbook=new XSSFWorkbook(file);

		int sheets=workbook.getNumberOfSheets();

		for(int i=0;i<sheets;i++) 
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("kar"))
			{
				XSSFSheet sheet=workbook.getSheetAt(0);

				Iterator<Row> rows=sheet.rowIterator();
				Row firstRow=rows.next();

				Iterator<Cell> cells=firstRow.cellIterator();

				int k=0;
				int coloumn=0;
				while(cells.hasNext())
				{
					Cell value=cells.next();
					if(value.getStringCellValue().equalsIgnoreCase("testcase"))
					{
						coloumn=k;
						
					}
					k++;
				}
				//System.out.println(coloumn);
				
				while (rows.hasNext())
				{
					Row r=rows.next();
					if(r.getCell(coloumn).getStringCellValue().equalsIgnoreCase(rowData))
					{
						Iterator<Cell> ce=r.cellIterator();
						
						while(ce.hasNext())
						{
							Cell c = ce.next();
							if(c.getCellTypeEnum()==CellType.STRING)
							{
								list.add(c.getStringCellValue());
								
							}
							else {
								list.add(NumberToTextConverter.toText(c.getNumericCellValue()));
								
							}
							
						}
					}
				}
			}
		}

		return list;
	}
}
