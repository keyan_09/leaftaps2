package week3.day2;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelSheetReadAndWrite {
	 private static XSSFWorkbook workBook;
	 private static XSSFSheet   sheet;
	 private static XSSFCell cell;
	// private static XSSFRow row;
	 public static FileInputStream file;

	//To Read Data from the ExcelSheet
	public static String getdata(String path, int rowNo, int Colno, String sheetName)throws Exception
	{
		try
		{
			file=new FileInputStream(path);
			
			workBook=new XSSFWorkbook(file);
			
			sheet=workBook.getSheet(sheetName);
			
			cell = sheet.getRow(rowNo).getCell(Colno);
			
		    String stringCellValue = cell.getStringCellValue();
		    
		    return stringCellValue;
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return "";
		}
	}

}
