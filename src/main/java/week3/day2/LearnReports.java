package week3.day2;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	public static void main(String[] args) throws IOException {

		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test = extent.createTest("TC00_CL", "Create a new Lead");
		test.assignAuthor("Gayatri");
		test.assignCategory("smoke");
		test.pass("Enter DemoSalesManger passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	    test.fail("Enter DemoSalesManger not passed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		extent.flush();
	
	
	
	
	
	
	
	
	
	
	
	}

}
