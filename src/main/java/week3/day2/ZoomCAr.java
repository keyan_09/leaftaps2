package week3.day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCAr {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'][1])").click();
		driver.findElementByClassName("proceed").click();
		String text = driver.findElementByXPath("(//div[@class='day'][1])").getText();
		System.out.println(text);
		driver.findElementByXPath("(//div[@class='day'][1])").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(2000);
		String text2 = driver.findElementByXPath("//div[@class='day picked ']").getText();
		System.out.println(text2);
		if (text.equals(text2)) {
			System.out.println("verified");
			
		} else {
			System.out.println("not verified");
			
		}
		driver.findElementByClassName("proceed").click();
		List<WebElement> ele = driver.findElementsByClassName("price");
		List<String> ele1=new ArrayList<>();
		
		for (WebElement webElement : ele) {
			System.out.println(webElement.getText());
			ele1.add(webElement.getText().replaceAll("\\D", ""));
		}
		String max = Collections.max(ele1);
		System.out.println("\n"+ max );
		
		
		String text3 = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
		Thread.sleep(3000);
		System.out.println(text3);
		
	
		

	}

}
