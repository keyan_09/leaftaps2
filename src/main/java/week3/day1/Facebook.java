package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Facebook {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").sendKeys("8190077109");
		driver.findElementById("pass").sendKeys("karthik09");
		driver.findElementById("loginbutton").click();
		driver.findElementByXPath("(//div[@class='innerWrap']//input[2])").sendKeys("testleaf",Keys.ENTER);
		Thread.sleep(2000);
		String text = driver.findElementByLinkText("TestLeaf").getText();
		//System.out.println(text);
		if (text.contains("TestLeaf")) {
			System.out.println("verified");
			
		} else {
			System.out.println("not verified");
		}
		Thread.sleep(2000);
		String text2 = driver.findElementByXPath("(//div[text()='TestLeaf']//following::button[1])").getText();
	//	System.out.println(text2);
		if (text2.equals("Like")) {
			driver.findElementByXPath("(//div[text()='TestLeaf']//following::button[1])").click();
			Thread.sleep(2000);
		} else {
			System.out.println("liked already");

		}
		driver.findElementByLinkText("TestLeaf").click();
		String title = driver.getTitle();
		//System.out.println(title);
		if (title.contains("testleaf")) {
			System.out.println("title verified");
			
		} else {
			System.out.println("title not verified");
				}
		String text3 = driver.findElementByXPath("//div[@class='clearfix _ikh']").getText();
		System.out.println(text3);
		
	}

}
