package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnSortable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/sortable/");
		driver.manage().window().maximize();
		driver.switchTo().frame(0);
		WebElement drag = driver.findElementByXPath("//*[@id=\"sortable\"]/li[1]");
		//WebElement des = driver.findElementByXPath("//*[@id=\"sortable\"]/li[1]");
		Point src=drag.getLocation();
		int x = src.getX();
		int y = src.getY();
		System.out.println("x value  "+x+"  y value  "+y);
		/*Actions builder=new Actions(driver);
		builder.dragAndDropBy(drag, 11, 8).perform();;
*/
	}

}
