package week3.day1;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class item {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://jqueryui.com/selectable/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		Actions act=new Actions(driver);
		WebElement a=driver.findElementByXPath("//li[text()='Item 1']");
		WebElement b=driver.findElementByXPath("//li[text()='Item 3']");
		WebElement c=driver.findElementByXPath("//li[text()='Item 5']");
		act.sendKeys(Keys.CONTROL).click(a).click(b).click(c).perform();
	}

}
