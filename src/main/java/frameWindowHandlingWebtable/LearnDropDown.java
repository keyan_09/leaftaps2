package frameWindowHandlingWebtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnDropDown {

	public static void main(String[] args) {
		
		String string="Selenium";
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://leafground.com/");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Drop down").click();
		
		WebElement s = driver.findElementByXPath("//select[@id='dropdown3']");
		Select source=new Select(s);
		/*String a=driver.findElementByXPath("//select[@id='dropdown3']").getText();
		System.out.println(a);
		List<String> list= new ArrayList<>();
		list.add(a);
		{
		for(int i=0;i<list.size();i++)
		if(list.get(i).equals(string))
		{
			System.out.println(list.get(i));
		}	*/
		
	

	Collection<WebElement> options = source.getOptions();
		
/*		
		Iterator<WebElement> itr = options.iterator();
	  //  itr.next();
		while(itr.hasNext()) {
			WebElement next = itr.next();
			System.out.println(next.getText());
		}
		
		
*/
for (WebElement webElement : options) {
	if(webElement.getText().equals(string)) {
		source.selectByVisibleText("Selenium");
		
	}
}

		WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
	webDriverWait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//select[@id='dropdown3']"), "Selenium"));
	
	}

}
