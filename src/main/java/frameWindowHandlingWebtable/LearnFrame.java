package frameWindowHandlingWebtable;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {
	

	public static void main(String[] args)
	{
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		 ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/frame.html");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.switchTo().frame(0);
		driver.findElement(By.xpath("//button[@id='Click']")).click();
		driver.switchTo().parentFrame();
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println(size);
		WebElement findElement2 = driver.findElement(By.xpath("//iframe[@src='page.html']"));
		driver.switchTo().frame(findElement2);
		WebElement findElement = driver.findElement(By.xpath("//iframe[@src='nested.html']"));
		driver.switchTo().frame(findElement);
		driver.findElement(By.xpath("//button[@id='Click1']")).click();
		
	}
}
