package frameWindowHandlingWebtable;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragandDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		 ChromeDriver driver=new ChromeDriver();
		driver.get("http://leafground.com/pages/drag.html");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		WebElement source = driver.findElement(By.xpath("//div[@class='ui-widget-content ui-draggable ui-draggable-handle']"));
		Actions act=new Actions(driver);
		act.dragAndDropBy(source, 100, 100).build().perform();
		
			driver.get("http://leafground.com/pages/selectable.html");
			WebElement findElement = driver.findElement(By.xpath("//li[contains(text(),'Item 1')]"));
			WebElement findElement1 = driver.findElement(By.xpath("//li[contains(text(),'Item 2')]"));
			act.sendKeys(Keys.CONTROL).click(findElement).click(findElement1).build().perform();;
	
		
		
		
	}

}
