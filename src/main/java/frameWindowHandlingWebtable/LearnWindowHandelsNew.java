package frameWindowHandlingWebtable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnWindowHandelsNew {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
		driver.get("http://leafground.com/pages/Window.html");
		driver.findElement(By.xpath("//button[contains(text(),'Open Home Page')]")).click();
		Set<String> window = driver.getWindowHandles();
		
		Iterator<String> iterator = window.iterator();
		String parent = iterator.next();
		System.out.println(parent);
		String child = iterator.next();
		
		driver.switchTo().window(child);
		
		
		driver.findElement(By.xpath("//a[@class='wp-categories-link maxheight'][1]/h5[contains(text(),'Edit')]")).click();
		driver.close();
		driver.switchTo().window(parent);
		driver.findElement(By.xpath("//button[contains(text(),'Open Multiple Windows')]")).click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> list= new ArrayList<>();
		list.addAll(windowHandles);
		System.out.println(list);
		driver.switchTo().window(list.get(2));
		System.out.println(driver.getTitle());
		driver.manage().window().maximize();
	driver.findElement(By.xpath("//button[@id='position']")).click();
	
	Actions a=new Actions(driver);
	
	
		
	}

}
