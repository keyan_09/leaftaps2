
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ExcelSheetReadAndWrite;

public class Ddfmodel {

	ChromeDriver driver;
	
	@BeforeSuite
	public void beforeSuite()
	{
		System.out.println("beforeSuite");
		//Actions act= new Actions(driver);
		
	}
	
	
	@BeforeClass
	public void beforeclass()
	{
		System.out.println("beforeclass");
	}
	
	@BeforeTest
	public void beforetest()
	{
		System.out.println("beforetest");
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforemethod");
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	 driver = new ChromeDriver();
	driver.get("http://leaftaps.com/opentaps");
	driver.manage().window().maximize();
	}
	
	
	@Test(dataProvider="fu")
	public void getdataFromexcel( String uname, String pass) throws InterruptedException
	{
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pass);
		driver.findElementByClassName("decorativeSubmit").click();
		System.out.println(driver.getTitle());
		Thread.sleep(2000);
		driver.findElement(By.linkText("CRM/SFA")).click();
		
	}
	
	@Test
	
	public void print1()
	{
		System.out.println("print1");
	}
	
	
@Test
	
	public void print2()
	{
		System.out.println("print2");
	}
	
	@AfterMethod
	public void afterMethod()
	{
		System.out.println("aftermethod");
		driver.quit();
	}
	
	@AfterClass
	
	public void afterClass()
	{
		System.out.println("afterClass");
		driver.quit();
	}
	
	
	
	@AfterTest
	
	public void afterTest()
	{
		System.out.println("afterTest");
		driver.quit();
	}
	
	
	@AfterSuite
	
	public void afterSuite()
	{
		System.out.println("afterSuite");
		driver.quit();
	}
	
	

	
	
	@DataProvider(name="fu")
	public Object[][] passdata()
	{
		try {
			new ExcelSheetReadAndWrite("E:\\selenium class\\workspace\\Selenium\\data\\karthik.xlsx", "shru");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		int row = ExcelSheetReadAndWrite.getrowno(1);
		Object[][] data=new Object[row][2];

		for(int i=0 ; i<row;i++)
		{
			try {
				data[i][0]=ExcelSheetReadAndWrite.getdata(i, 0);
				data[i][1]=ExcelSheetReadAndWrite.getdata(i, 1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return data;
	}
}
