package excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelSheetReadAndWrite {
	 private static XSSFWorkbook workBook;
	 private static XSSFSheet   sheet;
	// private static XSSFCell cell;
	 //private static XSSFRow row;
	 public static FileInputStream fileInput;
	 public static File src;

	 
	 public ExcelSheetReadAndWrite(String path,String sheetName) throws Exception
	 {
		 src=new File(path);
			
		 fileInput=new FileInputStream(src);
		 
		 workBook=new XSSFWorkbook(fileInput);
			
		sheet=workBook.getSheet(sheetName);
	 }
	//To Read Data from the ExcelSheet
	public static String getdata( int rowNo, int Colno )throws Exception
	{
		try
		{
			
			String stringCellValue = sheet.getRow(rowNo).getCell(Colno).getStringCellValue();
			
		    return stringCellValue;
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return "";
		}
	}
	
	public static  void setdata( int rowNo, int Colno,String value)throws Exception
	{
		try
		{
			
			sheet.getRow(rowNo).createCell(Colno).setCellValue(value);
			
			FileOutputStream wr= new FileOutputStream(src);
			
			workBook.write(wr);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			
		}
	}
	
	public static int getrowno(int sheetno)
	{
		int row=workBook.getSheetAt(sheetno).getLastRowNum();
		
		row=row+1;
	
		return row;
	}
	public static int getcollno(int sheetno)
	{
		int col=workBook.getSheetAt(sheetno).getRow(0).getLastCellNum();
	
		return col;
	}

}
