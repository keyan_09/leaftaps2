package screenshot;

import java.io.File;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

public class Utility {
	
	public static void screenshot(ChromeDriver driver) 
	{
		try {
			long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
	TakesScreenshot tss =(TakesScreenshot)driver;
		
		File screenshotAs = tss.getScreenshotAs(OutputType.FILE);
		
			FileUtils.copyFile(screenshotAs, new File("./snaps/"+ number+".png"));
		} catch (Exception e) {
			System.out.println("Exception while taking Screenshot"+e.getMessage());
			
		}
		
	}

}
