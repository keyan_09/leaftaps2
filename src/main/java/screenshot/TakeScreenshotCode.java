package screenshot;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;

public class TakeScreenshotCode {
	

	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		Utility.screenshot(driver);
		
		driver.findElement(By.id("username")).sendKeys("DemoSalesManager");
		Utility.screenshot(driver);
		
		TakesScreenshot tss =(TakesScreenshot)driver;
		
		File screenshotAs = tss.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(screenshotAs, new File("./snaps/shot.png"));
		
		System.out.println("screenshot taken");
		
		 Date objDate = new Date();
		 
		 
		// TimeStampService ts=(TimeStampService)driver;
		 
		 
		  System.out.println(objDate.toString());
	}

}
